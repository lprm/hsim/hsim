
# Manager settings
MQTT_TOPIC_MANAGER = "/CONFIG"
MQTT_HOST_MANAGER = os.getenv('MQTT_HOST', "192.168.0.244")
MQTT_PORT_MANAGER = os.getenv('MQTT_PORT', 30001)
URL_MANAGER = os.getenv('MANAGER_URL', "http://localhost:5000/api/v1/")


# Simulator settings
MQTT_TOPIC_SIMULATOR = "/CONFIG"
MQTT_HOST_SIMULATOR = os.getenv('MQTT_HOST', "localhost")
MQTT_PORT_SIMULATOR = os.getenv('MQTT_PORT', 1883)
URL_SIMULATOR = os.getenv('MANAGER_URL', "http://localhost:5000/api/v1/")
