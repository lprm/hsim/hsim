![alt text](resources/hsim_logo_100x62.png "HSim Logo")

## A scalable cloud-based tool for health-data simulation

HSim is a simulator for the evaluation of health applications in RPM. 

HSim aims to simplify the simulation of biomedical patient data to provide support for the assessment of infrastructures and services for healthcare applications. The implementation of HSim is divided into two main modules: the `manager` and `simulator`. 

The `manager` module contains the code structures responsible for managing the data repository requests and converting raw data to standard formats. The `simulator` module contains instructions on the code for the management and execution of the simulation and the respective availability of data for health applications.

## Running

The HSim project offers Docker images ready for use in its [Container Registry](https://gitlab.com/lprm/hsim/hsim/container_registry). Images for both the `manager` and the `simulator` are available in the registry

## Quick Start

The quickest way to get started is using [docker-compose](https://docs.docker.com/compose/).

```bash
wget https://gitlab.com/lprm/hsim/hsim/-/raw/master/docker-compose.yml
```

Start HSim using:

```bash
docker-compose up
```

Alternatively, you can manually launch the HSim containers (`manager` and `simulator`) and the supporting `emqx` containers by following this four step guide.

Step 1. Launch a EMQX container (to orchestrate communication between the `manager` and the `simulator`)

```bash
docker run --name emqx-manager -d \
    --publish 18883:1883 --publish 18083:18083 \
    emqx/emqx:latest
```

Step 2. Launch a EMQX container to receive simulation data:

```bash
docker run --name emqx -d \
    --publish 18884:1883 --publish 18084:18083 \
    emqx/emqx:latest
```

Step 3. Launch the `manager` container

```bash
docker run --name manager -d \
    --link emqx-manager:emqx-manager \
    --publish 5000:5000 \
    --env 'MQTT_HOST=emqx-manager' \
    --env 'MQTT_PORT=1883' \
    registry.gitlab.com/lprm/hsim/hsim:manager-0.2.8
```

Step 4. Launch the `simulator` container

```bash
docker run --name simulator -d \
    --link emqx-manager:emqx-manager \
    --link emqx:emqx \
    --link manager:manager \
    --env 'MQTT_HOST=emqx-manager' \
    --env 'MQTT_PORT=1883' \
    --env 'NUM_PROCS=5' \
    --env 'MANAGER_URL=http://manager:5000/api/v1/' \
    registry.gitlab.com/lprm/hsim/hsim:simulator-0.2.8
```

## Developing Locally

## 1. Clone the project

	git clone https://gitlab.com/lprm/hsim/hsim.git

## 2. Prepare the manager

### 2.1. Create a virtual environment

    python3 -m venv /path/to/env

### 2.2. Install Python dependencies

    source /path/to/env/bin/activate
    pip3 install -r manager/requirements.txt

### 2.3. Start the server

    python manager/main.py runserver

## 3. Prepare the simulator


### 3.1. Create a virtual environment

    python3 -m venv /path/to/env

### 3.2. Install Python dependencies

    source /path/to/env/bin/activate
    pip3 install -r simulator/requirements.txt

### 3.3. Start the simulator agent

    python simulator/agent.py
