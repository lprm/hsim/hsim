<!--#set var="TITLE" value="Fantasia Database"-->
<!--#set var="USELOCALCSS" value="1"-->
<!--#include virtual="/head.shtml"-->

<div class="notice">
<p>
This database is described in</p>

<div class="reference">
Iyengar N, Peng C-K, Morin R, Goldberger AL, Lipsitz LA. 
Age-related alterations in the fractal scaling of cardiac
interbeat interval dynamics. <i>Am J Physiol</i> 1996;<b>271</b>:1078-1084.
[<a href="http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&list_uids=8898003&dopt=Abstract" target="other">Abstract</a>]
</div> <!-- end reference -->

<p>
<b>Please cite this publication when referencing this material, and also
include the standard citation for PhysioNet:</b></p>
<div class="reference">
Goldberger AL, Amaral LAN, Glass L, Hausdorff JM, Ivanov PCh, Mark RG,
Mietus JE, Moody GB, Peng C-K, Stanley HE.  PhysioBank, PhysioToolkit, and
PhysioNet: Components of a New Research Resource for Complex Physiologic
Signals.
<i>Circulation</i> <b>101</b>(23):e215-e220 [Circulation Electronic Pages;
<a href="http://circ.ahajournals.org/content/101/23/e215.full"
target="other">http://circ.ahajournals.org/content/101/23/e215.full</a>];
2000 (June 13).
</div> <!-- end reference -->
</div>

<p>Twenty young (21 - 34 years old) and twenty elderly (68 - 85 years old)
rigorously-screened healthy subjects underwent 120 minutes of continuous supine
resting while continuous electrocardiographic (ECG), and respiration signals
were collected; in half of each group, the recordings also include an
uncalibrated continuous non-invasive blood pressure signal.  Each subgroup of
subjects includes equal numbers of men and women.</p>

<p>All subjects remained in a resting state in sinus rhythm while watching the
movie <a href="http://imdb.com/Title?0032455"
target="other"><i>Fantasia</i></a> (Disney, 1940) to help maintain
wakefulness. The continuous ECG, respiration, and (where available) blood
pressure signals were digitized at 250 Hz. Each heartbeat was
annotated using an automated arrhythmia detection algorithm, and each beat
annotation was verified by visual inspection.</p>

<p>Records <tt>f1y01, f1y02, ... f1y10</tt> and <tt>f2y01, f2y02, ... 
f2y10</tt>) were obtained from the young cohort, and records <tt>f1o01,
f1o02, ... f1o10</tt> and <tt>f2o01, f2o02, ... f2o10</tt>) were obtained from
the elderly cohort.  Each group of subjects includes equal numbers of men and
women.  Each record includes ECG (with beat annotations) and respiration, and
half of those in each group (the <tt>f2*</tt> records) include a blood pressure
waveform, as noted above. </p>

<p>
The beat annotations for a subset of these recordings were previously available
in a different format;  these are still available <a href="subset/">here</a>.</p>
