import time
import settings
import gzip
import json
import sys
# from repository.physionet.loader import PhysionetDataLoader
# from repository.physionet.settings_hdash_converter import physionet_hdash_converter
# from repository.physionet.settings_fhir_Converter import physionet_fhir_Converter
from repository.loader import HANDLER_MAPPING_LOADER
from repository.converter import HANDLER_MAPPING_CONVERTER
import uuid
import os
from support import erase_path
from apscheduler.schedulers.blocking import BlockingScheduler
from communication import mqtt


class ManagerConfig():

    def __init__(self, data_config):
        self.data_config = data_config
        settings.REPOSITORY = self.data_config["config_manager"]["repository"]

    def simulation_state(self):
        print("---------------------------------------------------")
        ss = settings.SimulationState
        if self.data_config["state"] == ss.PREPARE.name:
            self.prepare_simulation()
            # self.data_config["config_simulator"]["repeat"] = "* * * * * * * * 2019-11-05T17:45:00-03:00 *"
        elif self.data_config["state"] == ss.PAUSE.name or self.data_config["state"] == ss.RESUME.name or self.data_config["state"] == ss.STOP.name:
            print("Other States")
            self.states_simulation()
        else:
            print("None")

    def prepare_simulation(self):
        '''cria um identificador único para a simulação'''
        _uuid = uuid.uuid4().hex

        '''busca os dados localemente e no repositório espeficado'''
        handler_loader = HANDLER_MAPPING_LOADER[self.data_config["config_manager"]["repository"]]
        loader = handler_loader()
        loader.download(self.data_config["config_manager"]["database"])

        '''Lê localmente e converte os dados para o formato legível pela aplicação e coloca no formato fhir em json'''

        handler_converter = HANDLER_MAPPING_CONVERTER[self.data_config["config_simulator"]["template"]]
        converter = handler_converter()
        list_records = converter.convert(
            loader.get_records(self.data_config["config_manager"]["database"]), self)

        # print(list_records)
        try:
            if([] != list_records):
                print("* Publishing configuration of simulation")
                client = mqtt.on_connect()
                # print(json.dumps(self.data_config, indent=4))
                config_json = {"data_config": self.data_config,
                               "uuid": _uuid}
                '''Limpa a pasta temp DIR_PREPARED dos registros compactados'''
                erase_path(settings.DIR_PREPARED)

                '''Compacta os registros e armazena com a descrição uuid para ser baixado do lado simulador '''
                data = ""
                for rec in list_records:
                    data += json.dumps(rec)
                    data += "\n"

                with gzip.GzipFile(os.path.join(settings.DIR_PREPARED, "%s.gz" % (str(_uuid), )), 'w+', compresslevel=1) as fout:
                    fout.write(data.encode('utf-8'))
                # print(config_json)
                '''Envia as configurações do simulador e o uuid para o agent baixar os registros do lado simulador'''
                mqtt.on_publish(client, gzip.compress(json.dumps(
                    config_json).encode('utf-8'), compresslevel=1))

                print("* Published!")
                print("---------------------------------------------------")

            else:
                print("Error - empty list. Signal Type Not Found!")
        except Exception as e:
            print(e)

    def states_simulation(self):
        try:
            client = mqtt.on_connect()
            config_json = {"data_config": self.data_config,
                           "uuid": None}
            mqtt.on_publish(client, gzip.compress(json.dumps(
                config_json).encode('utf-8'), compresslevel=4))
        except Exception as e:
            print(e)
