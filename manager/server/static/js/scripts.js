// Empty JS for your own code to be here
var API_URL = URL_ROOT + "api/v1/";
var REPOSITORY = [null, $("#repositoryPhysionet"), $("#repositoryKaggle")];
var ELEMENTS = [null, $("#settingsInterval"), $("#settingsIntervalBetween"), $("#settingsIntervalSpecificTime"), $("#settingsExpression")];
var PROTOCOLS = [null, $("#settingsProtocolMqtt"), $("#settingsProtocolCoap"), $("#settingsProtocolWebSocket"), $("#settingsProtocolHTTP")];


var DATATYPE = [
    'ECG',
    'RESP',
    'BP',
    'CO2',
    'CO',
    'EEG',
    'EMG',
    'EOG',
    'HR',
    'MMG',
    'O2',
    'PLETH',
    'SCG',
    'STAT',
    'ST',
    'TEMP',
    'UNKNOWN'
];

REPEAT_SETTINGS_KEY = "repeat_settings";
TEMPLATE = ["JSON_FHIR", "XML_FHIR", "YAML_FHIR", "HDash_Schema"];
PROTOCOLS

$(document).ready(function () {
    $("#repository").click(function () {
        $("#newInput").attr("disabled", false);
    });
});

$(document).ready(function () {
    var max_fields = 2;
    var newInput = $("#newInput");
    var form_input = $("#input_settings"); // Fields wrapper
    var item = $(".form_input_settings");
    var x = 1;

    newInput.on("click", function () {
        if (x < max_fields) {
            x++;
            var novoItem = item.clone().removeAttr('id');
            novoItem.append('<a href="#" class="remove_field">Remove</a>')
            novoItem.attr("id", "form_input_settings" + x);
            form_input.append(novoItem);
            $("#newInput").attr("disabled", false);
        } else {
            $("#newInput").attr("disabled", true);
        }
    });
    $(document).on('click', ".remove_field", function () {
        $(this).parent('div').remove();
        x--;
    });
});


$(".protocols").on(":visible", function () {
    alert($(this).text());
});

$(document).ready(function () {
    var frm = $('#formConf');
    var state = null;

    $('.button_select').on('click', function () { // pega o clique dos botões de estatus
        state = event.target;
    });
    frm.submit(function (e) {
        e.preventDefault();
        // alert($(frm).text);

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify($(frm).serializeArray().map(function (x) { // trata as configurações de repetição
                if (!(REPEAT_SETTINGS_KEY in this)) {
                    this[REPEAT_SETTINGS_KEY] = {};
                }
                if (x.name.startsWith(REPEAT_SETTINGS_KEY)) {
                    this[REPEAT_SETTINGS_KEY][x.name.replace(REPEAT_SETTINGS_KEY + "_", "")] = x.value;
                } else {
                    this[x.name] = x.value;
                }
                this['state'] = state.textContent.replace(/\s+/g, '');
                return this;
            }.bind({}))[0]),
            success: function (data) {
                alert('Submission was successful');
                console.log('Submission was successful');
            },
            error: function (data) {
                alert("An error occurred.\n Verify the fields");
                console.log('An error occurred.');
            }
        });
    });
});

$("#dataType").on("change", function (value) {
    for (el of DATATYPE) {
        if (el != null) {
            el.addClass("d-none");
        }
    }
    var dataType = $(this).val();
    el = DATATYPE[dataType];
    if (el != null) {
        el.removeClass("d-none");
    }
});

$("#repository").on("change", function (value) {
    for (el of REPOSITORY) {
        if (el != null) {
            el.addClass("d-none");
        }
    }
    var repository = $(this).val();
    el = REPOSITORY[repository];
    if (el != null) {
        el.removeClass("d-none");
    }
});

$("#protocol").on("change", function (value) {
    for (el of PROTOCOLS) {
        if (el != null) {
            el.addClass("d-none");
        }
    }
    var protocol = $(this).val();
    el = PROTOCOLS[protocol];
    if (el != null) {
        el.removeClass("d-none");
    }
});

$("#repeat").on("change", function (value) {
    for (el of ELEMENTS) {
        if (el != null) {
            el.detach();
        }
    }
    var repeat = $(this).val();
    el = ELEMENTS[repeat];
    if (el != null) {
        el.appendTo("#repeat_settings");
        el.removeClass("d-none");
    }
});

$.getJSON(API_URL + "databases/", function (databases) {
    $.each(databases, function (key, value) {
        $("#database").append($("<option>").val(value.id).text(value.name));
    });
});


$.each(DATATYPE, function (i, value) {
    $('#dataType').append($('<option>', {
        value: value.id,
        text: value
    }));
});

$.each(TEMPLATE, function (i, value) {
    $('#dataFormat').append($('<option>', {
        value: value.id,
        text: value
    }));
});

// Input selects between times
for (i = 0; i < 24; i ++) {
    $('#repeat_settings_start').append($('<option>', {
        value: i,
        text: i + ":00"
    }));
}
for (i = 1; i < 24; i ++) {
    $('#repeat_settings_end').append($('<option>', {
        value: i,
        text: i + ":00"
    }));
}


// var updateInterval = 2000;
// setInterval(function () {
$.getJSON(API_URL + "statistic/", function (data) {
    var simulation = data["simulation"];
    var cluster = data["cluster"];

    var ctx = document.getElementById("chart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        responsive: true,
        data: {
            labels: simulation["labels"],
            datasets: [
                {
                    label: simulation["title"],
                    data: simulation["values"],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)'
                    ],
                    borderWidth: 2
                }
            ]
        },
        options: {
            scales: {
                yAxes: [
                    {
                        ticks: {
                            beginAtZero: true
                        }
                    }
                ]
            }
        }
    });
    // myChart.update({duration: 800, lazy: false, easing: 'easeOutBounce'});
    $("#conteires_use").text(cluster["containers"]);
    $("#m_use").text(cluster["m_use"] + "%");
    $("#cpu_use").text(cluster["cpu"] + "%");
    $("#netIO_use").text(cluster["net"]);
    $("#blockIO_use").text(cluster["block"]);
});

// }, updateInterval);
