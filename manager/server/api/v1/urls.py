from .resources import DatabasesResource, Statistic, ServeSimulation, TaskListAPI

urls = [
    ("/api/v1/databases/", DatabasesResource),
    ("/api/v1/statistic/", Statistic),
    ("/api/v1/simulation/<string:uuid>", ServeSimulation),
    ("/api/v1/tasks/", TaskListAPI)
]
