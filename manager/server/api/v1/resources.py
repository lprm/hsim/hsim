from flask_restful import Resource
# from repository.physionet.loader import PhysionetDataLoader
from repository.loader import HANDLER_MAPPING_LOADER
from flask import send_from_directory, request
from base.manager import ManagerConfig
import settings

handler_loader = HANDLER_MAPPING_LOADER["Physionet"]
dbs = handler_loader().list_databases()


class DatabasesResource(Resource):
    def get(self):
        # dbs = wfdb.get_dbs()
        global dbs
        databases = []
        for indice, d in enumerate(dbs[:len(dbs)-1]):
            db = {
                "id": indice,
                "name": d[0]+" - " + d[1]
            }
            databases.append(db)
        return databases


class ServeSimulation(Resource):
    def get(self, uuid):
        return send_from_directory(settings.DIR_PREPARED, "%s" % ((uuid), ), as_attachment=True)


class metadaConfig(Resource):
    def get(self, uuid):
        return send_from_directory(settings.DIR_PREPARED, "%s" % ((uuid), ), as_attachment=True)


class CronParser():
    CRON_PATTERN = ["*"] * 10

    def parse(self, type, **settings):
        pattern = self.CRON_PATTERN.copy()
        if type == 1 or type == 2:
            pos, val = self.parse_repeat(
                settings["interval"], settings["unit"])
            pattern[pos] = val
            # print(pattern[pos])
        if type == 2:
            pattern[5] = self.parse_between(settings["start"], settings["end"])
        if type == 3:
            pattern[5], pattern[6] = self.parse_time(settings["at"])
        if type == 4:
            pattern = settings["expression"]
            return pattern
        return " ".join(pattern)

    def parse_time(self, time):
        t = time.split(":")
        return (str(int(t[0])), str(int(t[1])))

    def parse_between(self, start, end=None):
        start_hour = int(start)
        if end:
            end_hour = int(end)
            return "%s-%s" % (start_hour, end_hour)
        else:
            return "%s" % (start_hour, )

    def parse_repeat(self, interval, unit):
        pattern = {"h": 5, "m": 6, "s": 7}
        return (pattern[unit[0]], "%s%s" % ("*/", interval))


class TaskListAPI(Resource):

    def post(self):
        global dbs
        json_data = request.get_json()
        # print(json_data)

        config_manager = {
            "repository": settings.REPOSITORIES[int(json_data["repository"])-1],
            "database": dbs[int(json_data["database"])][0],
            "dataType":  json_data["dataType"]
        }

        if settings.PROTOCOL[int(json_data["protocol"])-1] == settings.PROTOCOL[0]:
            # mqtt protocol
            protocol = {
                "protocol":  settings.PROTOCOL[int(json_data["protocol"])-1],
                "url": '%s://%s:%s%s' % (settings.PROTOCOL[int(json_data["protocol"])-1], json_data["MqttHost"],
                                         json_data["MqttPort"], json_data["MqttTopic"])
            }
        elif settings.PROTOCOL[int(json_data["protocol"])-1] == settings.PROTOCOL[1]:
            # coap protocol coap://localhost/time
            protocol = {
                "protocol":  settings.PROTOCOL[int(json_data["protocol"])-1],
                "url": '%s://%s:%s' % (settings.PROTOCOL[int(json_data["protocol"])-1], json_data["CoapHost"],
                                       json_data["CoapPort"])
            }
        elif settings.PROTOCOL[int(json_data["protocol"])-1] == settings.PROTOCOL[2]:
            # wbSocket protocol
            protocol = {
                "protocol":  settings.PROTOCOL[int(json_data["protocol"])-1],
                "url": '%s://%s:%s/%s' % (settings.PROTOCOL[int(json_data["protocol"])-1], json_data["WsHost"],
                                          json_data["WsPort"], json_data["WsProxy"])
            }
        elif settings.PROTOCOL[int(json_data["protocol"])-1] == settings.PROTOCOL[3]:
            # http protocol
            protocol = {
                "protocol":  settings.PROTOCOL[int(json_data["protocol"])-1],
                "url":  json_data["httpPort"]
            }
        else:
            print("* Protocol not found!")

        cron_parser = CronParser()
        cron = cron_parser.parse(
            int(json_data["repeat"]), **json_data["repeat_settings"])
        repeat = cron

        config_simulator = {
            # "connectNumber": json_data["connectionNumber"],
            "template":  json_data["dataFormat"],
            "protocol": protocol,
            "repeat":  repeat
        }
        data_config = {
            "state": json_data["state"].upper(),
            "config_manager": config_manager,
            "config_simulator": config_simulator,
        }

        print(data_config)
        manager = ManagerConfig(data_config)
        manager.simulation_state()


class Statistic(Resource):
    def get(self):
        labels = ["Prepared", "Paused", "Resumed", "Stopped"]
        values = [1000, 100, 200, 500]
        simulation = {
            "title": "Clients States",
            "_max": 17000,
            "labels": labels,
            "values": values
        }
        cluster = {
            "containers": 20,
            "m_use": 51,
            "cpu": 40,
            "net": 100,
            "block": 10
        }
        template = {
            "simulation": simulation,
            "cluster": cluster
        }
        return template
