from .views import IndexView

urls = [
    ("/", IndexView.as_view('index'))
]
