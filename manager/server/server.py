from flask import Flask
from flask_cors import CORS
import settings
from flask_restful import Resource, Api
from .api.v1.urls import urls as apiv1urls
from .urls import urls

app = Flask(__name__, static_url_path=settings.STATIC_URL,
            template_folder=settings.TEMPLATE_DIR, static_folder=settings.STATIC_DIR)
api = Api(app)
CORS(app)

for url, resource in apiv1urls:
    api.add_resource(resource, url)

for url, view in urls:
    app.add_url_rule(url, view_func=view)
