class BaseLoaderHandler():

    def __init__(self):
        pass

    def list_databases(self):
        raise NotImplementedError()

    def check_database(self, name):
        raise NotImplementedError()

    def download(self, name):
        raise NotImplementedError()

    def get_records(self, name):
        raise NotImplementedError()
