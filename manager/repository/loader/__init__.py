from repository.physionet.loader.physionet_loader import LoaderHandler as physionet_loader

HANDLER_MAPPING_LOADER = {
    'Physionet': physionet_loader
}
