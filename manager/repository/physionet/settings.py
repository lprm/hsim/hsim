import pandas as pd


fhir_ecg_map = {

    # MDC_PRESS_BLD_NONINV = 18498      # MDC_DIM_BEAT_PER_MIN = 2720
    # MDC_DIM_MMHG = 3872
    # MDC_DIM_MILLI_VOLT = 4274  # mV #
    # MDC_ECG_ELEC_POTL = 256

    # Unspecified Lead#
    "MLI": "MDwC_ECG_ELEC_POTL_I",  # = 257  # Lead I
    "MLII": "MDC_ECG_ELEC_POTL_II",  # = 258  # Lead II#
    "MLIII": "MDC_ECG_ELEC_POTL_III",  # = 317  # Lead III#
    # = 318  # Augmented voltage right (aVR)#
    "aVR": "MDC_ECG_ELEC_POTL_AVR",
    # = 319  # Augmented voltage left (aVL)#
    "aVL": "MDC_ECG_ELEC_POTL_AVL",
    # = 320  # Augmented voltage foot (aVF)#
    "aVF": "MDC_ECG_ELEC_POTL_AVF",
    "V1": "MDC_ECG_ELEC_POTL_V1",  # = 259  # Lead V1#
    "V2": "MDC_ECG_ELEC_POTL_V2",  # = 260  # Lead V2#
    "V3": "MDC_ECG_ELEC_POTL_V3",  # = 261  # Lead V3#
    "V4": "MDC_ECG_ELEC_POTL_V4",  # = 262  # Lead V4#
    "V5":  "MDC_ECG_ELEC_POTL_V5",  # = 263  # Lead V5#
    "V6": "MDC_ECG_ELEC_POTL_V6",  # = 264  # Lead V6#
}

unit_scale = {
    'voltage': ['pV', 'nV', 'uV', 'mV', 'V', 'kV'],
    'temperature': ['C', 'F'],
    'pressure': ['mmHg'],
    'no_unit': ['NU'],
    'percentage': ['%'],
    'heart_rate': ['bpm'],
}

# # index=['bp', 'co2', 'co', 'ecg', 'eeg', 'emg', 'eog', 'hr', 'mmg',
#        'o2', 'pleth', 'resp', 'scg', 'stat', 'st', 'temp', 'unknown'],
SIGNAL_CLASSES = pd.DataFrame(
    index=['BP', 'CO2', 'CO', 'ECG', 'EEG', 'EMG', 'EOG', 'HR', 'MMG',
           'O2', 'PLETH', 'RESP', 'SCG', 'STAT', 'ST', 'TEMP', 'UNKNOWN'],
    columns=['description', 'unit_scale', 'signal_names'],
    data=[['Blood Pressure', 'pressure', ['bp', 'abp', 'pap', 'cvp']],  # bp
          ['Carbon Dioxide', 'percentage', ['co2', 'pco2']],  # co2
          ['Carbon Monoxide', 'percentage', ['co']],  # co
          ['Electrocardiogram', 'voltage', [
              'i', 'ii', 'iii', 'iv', 'v', 'avr']],  # ecg
          ['Electroencephalogram', 'voltage', ['eeg']],  # eeg
          ['Electromyograph', 'voltage', ['emg']],  # emg
          ['Electrooculograph', 'voltage', ['eog']],  # eog
          ['Heart Rate', 'heart_rate', ['hr']],  # hr
          ['Magnetomyograph', 'voltage', ['mmg']],  # mmg
          ['Oxygen', 'percentage', ['o2', 'spo2']],  # o2
          ['Plethysmograph', 'pressure', ['pleth']],  # pleth
          ['Respiration', 'no_unit', ['resp']],  # resp
          ['Seismocardiogram', 'no_unit', ['scg']],  # scg
          ['Status', 'no_unit', ['stat', 'status']],  # stat
          ['ST Segment', '', ['st']],  # st. This is not a signal?
          ['Temperature', 'temperature', ['temp']],  # temp
          ['Unknown Class', 'no_unit', []],  # unknown. special class.
          ]
)

fhir_observation_category = pd.DataFrame(
    columns=["Code", "Display"],
    data=[["social-history", "Social History"],
          ["vital-signs", "Vital Signs"],
          ["imaging", "Imaging"],
          ["laboratory", "Laboratory"],
          ["procedure", "Procedure"],
          ["survey", "Survey"],
          ["exam", "Exam"],
          ["therapy", "Therapy"],
          ["activity", "Activity"]]
)

code_loinc = [
    ["Vital Signs Panel", 85353-1],
    ["Respiratory Rate", 9279-1],
    ["Heart rate", 8867-4],
    ["Oxygen saturation", 2708-6],
    ["Body temperature", 8310-5],
    ["Body height", 8302-2],
    ["Head circumference", 9843-4],
    ["Body weight", 29463-7],
    ["Body mass index", 39156-5],
    ["Blood pressure systolic and diastolic", 85354-9],
    ["Systolic blood pressure", 8480-6],
    ["Diastolic blood pressure", 8462-4]
]


def dictionary_physionet(key):
    '''
        busca pelo código do tipo de registro no dicionário
    '''
    if key in fhir_ecg_map:
        return fhir_ecg_map[key]
    elif key in SIGNAL_CLASSES.index:
        return SIGNAL_CLASSES["description"][key]
    else:
        return key


def dictionary_physionet_type(key):
    '''
        busca pelo código do tipo de registro no dicionário
    '''
    # print(key in SIGNAL_CLASSES.index)
    if key in fhir_ecg_map:
        return "ECG"
    elif key in SIGNAL_CLASSES.index:
        return key
    else:
        return key
