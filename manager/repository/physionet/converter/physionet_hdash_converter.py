import settings
import json
from repository.converter.base import BaseConverterHandler
from repository.physionet.settings import dictionary_physionet, dictionary_physionet_type
import os 
import datetime 
import copy


TEMPLATES = {
    "heart_rate": {
        "schema": "http://healthdash.com.br/heart_rate_measurement/#1",
        "measurement": {
            "bpm": 118
        },
        "date": "2019-11-08 13:42:02.224876+00:00",
        "data": None,
        "user": None
    },
    "RESP": {
        "data": {
            "schema": "http://healthdash.com.br/respiratory_minute_volume_measurement/#1",
            "measurement": {
                "mv": None
            },
            "date": None,
        },
        "user": None
    }
}


class ConverterHandler(BaseConverterHandler):
    id_record = None
    signals = None
    fields = None
    manager = None

    def format_record(self, type_, date, mv):
        temp = copy.deepcopy(TEMPLATES[type_])
        temp["data"]["date"] = date.isoformat()
        temp["user"] = 1
        temp["data"]["measurement"]["mv"] = mv
        return temp

    def convert_record(self, record_db, manager):
        fhir_list = []
        self.manager = manager
        self.id_record, self.signals, self.fields = record_db
        for indice, _type in enumerate(self.fields["sig_name"]):
            if(dictionary_physionet_type(_type) == self.manager.data_config["config_manager"]["dataType"]):
                for i, r in enumerate(self.signals[:, indice].tolist()[:1]):
                    ms = ((60/self.fields['fs'])*1000)*((indice + 1) * (i+1))
                    date = (datetime.datetime.now() +
                            datetime.timedelta(milliseconds=ms))
                    data = self.format_record(
                        self.manager.data_config["config_manager"]["dataType"], date, r)
                    fhir_list.append(data)

        return fhir_list
