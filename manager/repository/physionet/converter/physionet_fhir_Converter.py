import settings
import json
from repository.converter.base import BaseConverterHandler
from repository.physionet.settings import dictionary_physionet, dictionary_physionet_type
import os 


class ConverterHandler(BaseConverterHandler):
    id_record = None
    signals = None
    fields = None
    manager = None

    def fhir_template_base(self):    # retorna a parte inicial do dict fhir
        '''
            Retorna o cabeçalho do fhir
        '''
        fhir = {
            "resourceType": "Observation",
            "id": self.id_record.split("/")[-1].split(".")[0],
            "source": self.id_record.split("/")[-2],
            "type": self.manager.data_config["config_manager"]["dataType"],
            "text": self.fields["comments"],
            "status": "final",
            "category":
                {
                    "coding": {
                        "system": "http://terminology.hl7.org/CodeSystem/observation-category",
                        "code": "vital-signs",
                                "display": "Vital Signs"
                    },
                    "text": "Vital Signs"
            },
            "code": {
                "coding": {
                    "system": "http://loinc.org",
                    "code": "85353-1",
                    "display": "Vital signs, weight, height, head circumference, oxygen saturation and BMI panel"
                },
                "text": "Vital signs Panel"
            },
            "note":  self.fields["sig_name"],
            "component": []
        }

        return fhir

    def fhir_template_component(self, list_signal, indice):
        '''
            retorna o componente do fhir no formato dict
        '''
        component = {
            "sig_name": self.fields["sig_name"][indice],
            "valueQuantity": len(list_signal),
            "code": {
                "coding": ""
            },
        }
        component["code"]["coding"] = {
            "system": "urn:oid:2.16.840.1.113883.6.24",
            "code": "131329",
            "display": dictionary_physionet(self.fields["sig_name"][indice])
        }
        component["valueSampledData"] = {
            "frequency": self.fields["fs"],
            "unit": self.fields["units"][indice],
            "dimensions": 1,
            "data": list_signal
        }
        return component

    def create_fhir_file(self):
        '''
                Cria o cabeçalho do arquivo fhir em json
                e adiciona os componentes de acordo com a
                quantidade de sinais observados
        '''
        fhir = self.fhir_template_base()
        for indice, _type in enumerate(self.fields["sig_name"]):

            if(dictionary_physionet_type(_type) == self.manager.data_config["config_manager"]["dataType"]):
                fhir["component"].append(self.fhir_template_component(
                    self.signals[:, indice].tolist(), indice))
        # print(self.convert_record(record, manager) is not None)
        # print(fhir["component"])
        return fhir

    def convert_record(self, record_db, manager):
        '''
            Converte arquivo .dat para Json no padrão FHIR
        '''
        self.manager = manager
        self.id_record, self.signals, self.fields = record_db
        fhir_list = self.create_fhir_file()

        return fhir_list
