from repository.loader.base import BaseLoaderHandler
import settings
import os.path
import glob
import wfdb


class LoaderHandler(BaseLoaderHandler):

    def __init__(self):
        self.databases = None
        self._databases_cache_list = os.path.join(
            settings.DIR_SOURCE, "lista_db_physionet.txt")

    def _load_databases(self):
        dbs = wfdb.get_dbs()
        with open(self._databases_cache_list, 'w') as f:
            for d in dbs:
                f.write("%s\n" % d)

    def list_databases(self):
        ''' List the physionet Databases  '''
        print("* Listing databases...")
        if not self.databases:
            self.databases = [x.split(";") for x in open(self._databases_cache_list,
                                                         'r').read().split('\n')]
        return self.databases

    def check_database(self, nameDB):
        try:
            found = any(nameDB in sublist for sublist in self.databases)
            return found
        except Exception as e:
            return e

    def download(self, nameDB):
        '''Download WFDB record as requested by the user'''

        if(self.check_database(nameDB)):
            path = os.path.join(settings.DIR_SOURCE, nameDB)

            # Verifica se todos os registros daquele banco estão armazenados localmente, senão baixa tudo
            # if os.path.exists(path) and self.verify_records_exist(nameDB) == []:
            if os.path.exists(path):
                print("* Repository " + path.split("/")
                      [-1] + " is already exist localy")
            else:
                print("* Downloading database", nameDB)
                # wfdb.dl_database(nameDB, path)
                os.system("rsync -av -P -CaLvz physionet.org::"+nameDB+" "+path)


    def get_records(self, nameDB):
        # self._load_databases()
        path = os.path.join(settings.DIR_SOURCE, nameDB)
        if os.path.exists(path):
            try:
                files = glob.glob("%s/*.dat" % (path, ))
                record = []
                for fname in files[:settings.NUMBER]:
                    record_ = wfdb.rdsamp(fname.replace(".dat", ""))
                    record = (fname, record_[0], record_[1])
                    yield record
                print("* Readed directory " + path.split("/")[-1])
            except Exception as e:
                print(e)
        else:
            print("Records is not exist", nameDB)

    def verify_records_exist(self, database):
        '''Verifica se todos os registros daquele banco estão localmente'''
        path = os.path.join(settings.DIR_SOURCE, database)
        lista_arquivo_1 = wfdb.get_record_list(database)
        lista = glob.glob("%s/*.dat" % (path, ))
        lista_arquivo_2 = [l.split("/")[-1].split(".")[0]
                           for l in lista]
        lista_final = [
            x for x in lista_arquivo_1 if x not in lista_arquivo_2]
        return lista_final

    def download_record(self, database, sampFrom=None, sampTo=None):
        '''Baixa um registro especifico do banco'''
        path = os.path.join(settings.DIR_SOURCE, database)
        f_names = wfdb.get_record_list(database)
        if os.path.exists(path):
            print("Downloading records from", database)
            list_not_exist = self.verify_records_exist(database)
            for name_record in list_not_exist:
                # wfdb.dl_database(database, path, records=[name_record])
                wfdb.dl_files(database, path, [name_record + ".dat"])
                print("* Downloadead ", name_record)
            print("* Downloadead ", database)
        else:
            print("*Downloading all records from ", database)
            self.download_db(database)
