from repository.physionet.converter.physionet_fhir_Converter import ConverterHandler as FhirCommunicationHandler
from repository.physionet.converter.physionet_hdash_converter import ConverterHandler as HDashCommunicationHandler

HANDLER_MAPPING_CONVERTER = {
    'JSON_FHIR': FhirCommunicationHandler,
    'HDash_Schema': HDashCommunicationHandler,
}
