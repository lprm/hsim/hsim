

class BaseConverterHandler():

    def convert(self, records, manager):
        list_records = []
        for record in records:
            _converted = self.convert_record(record, manager)
            list_records.append(_converted)
        print("* Data converted to FHIR!")
        return list_records

    def convert_record(self, record, manager):
        raise NotImplementedError()
