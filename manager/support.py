import os.path
import glob


def erase_path(path_file):
    '''
        Método utilizado para excluir todos os arquivos do diretório
    '''
    if os.path.exists:
        file_name = glob.glob("%s/*.*" % (path_file, ))
        for fname in file_name:
            os.remove(fname)
