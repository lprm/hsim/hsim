import os.path
from enum import Enum
# from settings_all


class SimulationState(Enum):
    PREPARE = 1
    RESUME = 2
    STOP = 3
    PAUSE = 4


'''
    Diretórios utilizadas para armazenar os registros no
    manager
'''
REPOSITORY = ""
NUMBER = 5

APP_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))
DIR_SOURCE = os.path.join(
    APP_DIR, 'data', 'manager_data', 'data_source')
DIR_PREPARED = os.path.join(APP_DIR, 'data', 'manager_data', 'prepared')

'''
    Configurações do estáticas do server
'''
STATIC_URL = "/static"
STATIC_DIR = os.path.join(APP_DIR, 'server', 'static')
TEMPLATE_DIR = os.path.join(APP_DIR, 'server', 'templates')
URL = os.getenv('MANAGER_URL', "http://localhost:5000/api/v1/")

# URL = settings_all.URL_MANAGER


'''
    Configurações para conectar os dois ambientes
'''
# MQTT_TOPIC = "/CONFIG"
# MQTT_HOST = os.getenv('MQTT_HOST', "localhost")
# MQTT_PORT = os.getenv('MQTT_PORT', 1883)

MQTT_TOPIC = "/CONFIG"
MQTT_HOST = os.getenv('MQTT_HOST', "192.168.0.244")
MQTT_PORT = int(os.getenv('MQTT_PORT', 30001))


# MQTT_HOST = settings_all.MQTT_HOST_MANAGER
# MQTT_PORT = settings_all.MQTT_PORT_MANAGER


'''
    Configurações possíveis para o ambiente simulado
'''

REPOSITORIES = ["Physionet", "Kaggle"]
PROTOCOL = ["mqtt",  "coap", "wsocket", "http"]
FORMAT = ["json", "xml", "yaml"]
'''
    Configurações de teste para o ambiente simulado
'''

'''
# STATE = 1
# db = ["mitdb", "fantasia", "wrist"]
# list_type = ["ECG", "RESP"]
# form = FORMAT[0]
'''
