import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import settings


def on_connect():
    client = None
    '''client = mqtt.Client()
    client.subscribe(settings.MQTT_TOPIC)
    client.connect(settings.MQTT_HOST, settings.MQTT_PORT)
    print("* Connected:" + "Host:"+settings.MQTT_HOST + ";Topic:" + settings.MQTT_TOPIC +
          ";Port:" + str(settings.MQTT_PORT))'''
    return client


def on_publish(client, config_byte):
    publish.single(settings.MQTT_TOPIC, config_byte, hostname=settings.MQTT_HOST, port=settings.MQTT_PORT, qos=2)

    '''client = mqtt.Client()
    #client.subscribe(settings.MQTT_TOPIC)
    client.connect(settings.MQTT_HOST, settings.MQTT_PORT)
    print("* Connected:" + "Host:"+settings.MQTT_HOST + ";Topic:" + settings.MQTT_TOPIC +
          ";Port:" + str(settings.MQTT_PORT))
    return client.publish(settings.MQTT_TOPIC, payload=config_byte, qos=2)'''
