
def runserver():
    from server import app
    app.run(host='0.0.0.0', debug=True)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="subparser")
    runserver_command = subparsers.add_parser('runserver')
    kwargs = vars(parser.parse_args())
    globals()[kwargs.pop('subparser')]()
