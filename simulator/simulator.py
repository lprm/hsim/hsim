from communication import HANDLER_MAPPING
from urllib.parse import urlparse
from scheduler import scheduler
import settings
import random
from itertools import (takewhile, repeat)
import linecache
import json
from logger import logger


class SimulatorManager():
    handler = None
    records = None
    trigger = None
    template = None

    def __init__(self, records, data_config):

        self.job = None
        self.state = data_config["state"]
        self.template = data_config["config_simulator"]["template"]
        self.protocol = data_config["config_simulator"]["protocol"]
        self.trigger = data_config["config_simulator"]["repeat"]
        self.records = records
        handler_class = HANDLER_MAPPING[self.protocol["protocol"]]
        self.handler = handler_class(
            url=urlparse(self.protocol["url"]))

        print(data_config)
        print("* Init sucessfull")

    def rawincount(self, filename):
        f = open(filename, 'rb')
        bufgen = takewhile(lambda x: x, (f.raw.read(1024*1024)
                                         for _ in repeat(None)))
        return sum(buf.count(b'\n') for buf in bufgen)

    async def _run(self):
        try:
            print("* Running")
            await self.handler.init_connection()
            # _list = ""
            # print(self.rawincount(self.records))
            pos = random.randint(1, self.rawincount(self.records))
            # print(pos)
            print("buscando --> ")
            # print(linecache.getline(self.records, pos))
            record = json.loads(linecache.getline(self.records, pos))

            # print(self.records.loc[pos, :])

            # record = self.records.loc[pos, :]
            r = random.choice(record)
            # print(r)
            await self.handler.send_data(r)

            await self.handler.close_connection()
        except Exception as e:
            logger.capture_exception(e)

        # for record in self.records:
        #     print((self.records))
        #     for r in record:
        #         print(r)
        #         await self.handler.send_data(r)
        # for record in self.records:
        #     print((self.records))
        #     for r in record:
        #         print(r)
        #         await self.handler.send_data(r)
        #         #_list += ", ".join(record["id"])
        #         #_list += ", ".join(record["id"])

    async def run(self):
        print("* trigger")
        _state = settings.SimulationState
        if self.state == _state.PREPARE.name:
            self.job = scheduler.add_job(self._run, self.trigger)
            await scheduler.start()
        elif self.state == _state.PAUSE.name:
            self.job = scheduler.pause_job(self._run)
        elif self.state == _state.RESUME.name:
            self.job = scheduler.resume_job(self._run)
        elif self.state == _state.STOP.name:
            scheduler.stop_job()
