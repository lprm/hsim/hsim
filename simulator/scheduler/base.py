import iso8601
from apscheduler.triggers.cron import CronTrigger
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from pytz import utc


class TriggerParser():

    def __init__(self):
        pass

    def parse(self, expression):
        trigger = expression.split()

        # Verificar
        # if trigger[5] != "*":
        #     trigger[5] = "*/"+trigger[5]
        # elif trigger[6] != "*":
        #     trigger[6] = "*/"+trigger[6]
        # elif trigger[7] != "*":
        #     trigger[7] = "*/"+trigger[7]

        if trigger[5] != "*":
            trigger[5] = trigger[5]
        elif trigger[6] != "*":
            trigger[6] = trigger[6]
        elif trigger[7] != "*":
            trigger[7] = trigger[7]

        trigger = {
            "year": trigger[0],
            "month": trigger[1],
            "day": trigger[2],
            "week": trigger[3],
            "day_of_week": trigger[4],
            "hour": trigger[5],
            "minute": trigger[6],
            "second": trigger[7],
            "start_date": self.parse_date(trigger[8]),
            "end_date": self.parse_date(trigger[9]),
        }
        return trigger

    def parse_date(self, date):
        if (date == "*"):
            return None
        else:
            iso8601.parse_date(date)
            return date


class JobScheduler():

    def __init__(self):
        self.scheduler = AsyncIOScheduler(timezone=utc)
        self.trigger_parser = TriggerParser()
        self.list_job = None

    def add_job(self, function, trigger):
        if len(self.scheduler.get_jobs()) != 0:
            print("Clear jobs")
            self.scheduler.shutdown(0)
            self.scheduler = AsyncIOScheduler(timezone=utc)
        trigger_kwargs = self.trigger_parser.parse(trigger)
        trigger = CronTrigger(**trigger_kwargs)
        self.scheduler.add_job(function, trigger)

    def pause_job(self, function):
        if len(self.scheduler.get_jobs()) != 0:
            print("* Task pausing")
            self.scheduler.pause()
            print("* Task paused")
        else:
            print("* Not possible to paused. List of tasks is empty!")

    def resume_job(self, function):
        if len(self.scheduler.get_jobs()) != 0:
            print("* Task resuming")
            self.scheduler.resume()
            print("* Task resumed")
        else:
            print("* Not possible to resumed. List of tasks is empty!")

    def stop_job(self):
        if len(self.scheduler.get_jobs()) != 0:
            print(self.scheduler.shutdown(0))
            print("* Task removed")
        else:
            print("* Not possible to stopped.List of tasks is empty!")

    async def start(self):
        self.scheduler.start()


scheduler = JobScheduler()
