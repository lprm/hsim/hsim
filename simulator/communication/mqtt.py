from hbmqtt.client import MQTTClient, ClientException
from .base import BaseCommunicationHandler
from urllib.parse import urlparse
import json
import asyncio


class CommunicationHandler(BaseCommunicationHandler):
    client = None
    url = None

    async def init_connection(self):
        self.client = MQTTClient()
        self.url = self.settings.get(
            "url", urlparse("mqtt://localhost:1883/topic"))
        await self.client.connect('mqtt://%s:%s/' % (self.url.hostname or "localhost", self.url.port or 1883))

    async def send_data(self, data):
        topic = self.url.path
        await self.client.publish(topic, json.dumps(data).encode(), qos=2)

    async def close_connection(self):
        await self.client.disconnect()