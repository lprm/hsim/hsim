import paho.mqtt.client as mqtt
from .base import BaseCommunicationHandler
from urllib.parse import urlparse
import websockets


class CommunicationHandler(BaseCommunicationHandler):
    client = None
    url = None

    async def init_connection(self):
        self.url = self.settings.get("url", urlparse("ws://localhost:1883/ws"))
        self.client = await websockets.connect(self.url.geturl(),
                                               subprotocols=["auth.token", "auth.token.value.NHlKc282QzBCZ0JsdzNFTjlYd09reTJOekF2d0R0"])

    async def send_data(self, data):
        await self.client.send(str(data))
        print("Published:"+self.url)
