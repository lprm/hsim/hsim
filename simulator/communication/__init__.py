from .coap import CommunicationHandler as COAPCommunicationHandler
from .mqtt import CommunicationHandler as MQTTCommunicationHandler
from .ws import CommunicationHandler as WSCommunicationHandler
from .http import CommunicationHandler as HTTPCommunicationHandler

HANDLER_MAPPING = {
    'mqtt': MQTTCommunicationHandler,
    'coap': COAPCommunicationHandler,
    'ws': WSCommunicationHandler,
    'http': HTTPCommunicationHandler
}
