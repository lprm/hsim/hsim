from .base import BaseCommunicationHandler
from aiocoap import Context, Message, PUT
from urllib.parse import urlparse
import asyncio


class CommunicationHandler(BaseCommunicationHandler):
    context = None
    url = None

    async def init_connection(self):
        self.context = await Context.create_client_context()
        self.url = self.settings["url"]

        self.url = self.settings.get(
            "url", urlparse("coap://localhost:1883"))

    async def send_data(self, data):
        # request = Message(code=PUT, payload=str(data),
        #                   uri=self.url.geturl())
        # response = await self.context.request(request).response

        # print('Result: %s\n%r' % (response.code, response.payload))
        # print("Published:"+self.url)

        try:
            request = Message(code=PUT, payload=str(data).encode(),
                              uri=self.url.geturl())
            response = await self.context.request(request).response
        except Exception as e:
            print('Failed to fetch resource:')
            print(e)
        else:
            print('Result: %s\n%r' % (response.code, response.payload))
            print("Published:"+self.url)
