import paho.mqtt.client as mqtt
from .base import BaseCommunicationHandler
from urllib.parse import urlparse
import requests
import json


class CommunicationHandler(BaseCommunicationHandler):
    client = None

    async def init_connection(self):
        self.url = self.settings.get(
            "url", urlparse("http://localhost:8000/api/"))
        self.headers = {
            "Authorization": "Bearer TtFPlnbFQeMf8wViON2SildkrNpkle"}

    async def send_data(self, data):
        print(data)
        resp = requests.post(self.url.geturl(), headers=self.headers,
                             json=data)
        print(resp.text)
