class BaseCommunicationHandler():
    settings = {}

    def __init__(self, **settings): 
        self.settings = settings

    def init_connection(self):
        raise NotImplementedError

    def send_data(self, data):
        raise NotImplementedError

    def close_connection(self):
        pass
