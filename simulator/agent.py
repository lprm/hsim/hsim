import json
import gzip
import settings
from hbmqtt.client import MQTTClient, ClientException
from hbmqtt.mqtt.constants import QOS_2
import sys
from simulator import SimulatorManager
import requests
import os
import logging
from support import erase_path
import asyncio
import io
import traceback


logging.basicConfig(
    level=logging.INFO,
    handlers=[
        logging.StreamHandler(sys.stdout)
    ])


class AgentConfig():
    state = None
    config = None
    data_list = None
    uuid = None
    client = None
    loop = None

    def __init__(self, loop):
        self.loop = loop

    async def start_mqtt_agent(self):
        self.client = MQTTClient()
        await self.client.connect('mqtt://%s:%s/' % (settings.MQTT_HOST, settings.MQTT_PORT))
        print("* Connected on mqtt")
        await self.client.subscribe([
            (settings.MQTT_TOPIC, QOS_2),
        ])
        print("* Subscribed on topic")
        await self.client.publish(settings.MQTT_TOPIC_CONFIGURED, "OK".encode(), QOS_2)
        try:
            while True:
                message = await self.client.deliver_message()
                packet = message.publish_packet
                print("* Data received on topic %s" %
                      (packet.variable_header.topic_name, ))
                await self.process_message(packet.payload.data)
        except ClientException as ce:
            logger.error("Client exception: %s" % ce)

    async def load_settings(self, simulation_id):
        s = requests.Session()
        a = requests.adapters.HTTPAdapter(max_retries=settings.MANAGER_MAX_RETRIES)
        s.mount('http://', a)
        s.mount('https://', a)
        r = s.get(os.path.join(os.path.join(
            settings.URL, "simulation/", "%s.gz" % simulation_id)), timeout=settings.MANAGER_TIMEOUT)
        return r

    async def process_message(self, msg):

        try:
            ''' Recebe os comandos e o código uuid da URL'''

            logging.info("* Received Message")
            data_config_all = json.loads(
                gzip.decompress(msg).decode("utf-8"))

            self.data_config = data_config_all["data_config"]
            self.uuid = data_config_all["uuid"]

            '''Faz a requisição dos dados, por meio do acesso a URL com UUID especifico'''
            if self.uuid != None:
                response = await self.load_settings(self.uuid)
                if response.status_code == 200:
                    logging.info('* Success!')
                    file_name = os.path.join(settings.DIR_FILE, self.uuid)
                    f_in = gzip.decompress(response.content)
                    erase_path(settings.DIR_FILE)
                    with open(file_name, 'wb') as f_out:
                        f_out.write(f_in)

                    self.data_list = file_name
                elif response.status_code == 404:
                    logging.info('Not Found.')
            else:
                self.data_list = None

            # await self.simulation_state()
            await self.prepare_simulation()
            logging.info("* Waiting message")
            await self.client.publish(settings.MQTT_TOPIC_SUCCESS, "OK".encode(), QOS_2)
        except Exception as e:
            logging.info(e)
            err = traceback.format_exc()
            await self.client.publish(settings.MQTT_TOPIC_ERROR, err.encode(), QOS_2)

    async def prepare_simulation(self):
        '''
            Encaminha os dados para o gerenciador de configuração
        '''
        print("---------------------------------------------------")
        logging.info("* Setting")
        # if self.data_list != None:
        #     save_as_zip(self.data_config, self.data_list)
        simulador = SimulatorManager(self.data_list, self.data_config)
        await simulador.run()
        print("---------------------------------------------------")

    async def run(self):
        asyncio.ensure_future(self.start_mqtt_agent())


if __name__ == '__main__':
    print("---------------------------------------------------")
    loop = asyncio.get_event_loop()
    agent = AgentConfig(loop)
    loop.run_until_complete(agent.run())
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))
    try:
        asyncio.get_event_loop().run_forever()
    except (KeyboardInterrupt, SystemExit):
        pass
