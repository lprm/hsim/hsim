import os
from enum import Enum
# import .settings_all


class SimulationState(Enum):
    PREPARE = 1
    RESUME = 2
    STOP = 3
    PAUSE = 4


MQTT_TOPIC = "/CONFIG"
MQTT_TOPIC_SUCCESS = "/SUCCESS"
MQTT_TOPIC_ERROR = "/ERROR"
MQTT_TOPIC_CONFIGURED = "/CONFIGURED"
MQTT_HOST = os.getenv('MQTT_HOST', "localhost")
MQTT_PORT = os.getenv('MQTT_PORT', 1883)

# MQTT_HOST = settings_all._SIMULATOR
# MQTT_PORT = settings_all._SIMULATOR

# URL = "%s%s%s" % ("http://", "MQTT_HOST", ":5000/api/v1/")
# URL = "%s%s%s" % ("http://", "192.168.0.105", ":5000/api/v1/")
# URL = "http: // localhost: 5000/api/v1 /"
URL = os.getenv('MANAGER_URL', "http://localhost:5000/api/v1/")
MANAGER_MAX_RETRIES = os.getenv('MANAGER_MAX_RETRIES', 10)
MANAGER_TIMEOUT = os.getenv('MANAGER_TIMEOUT', 30)

SENTRY_DSN = os.getenv('SENTRY_DSN', None)

# URL = settings_all.URL_SIMULATOR

APP_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))

DIR_FILE = os.path.join(APP_DIR, 'data/')
