import settings
import glob
import os
import json
import gzip
import logging


def save_as_zip(data_config, data_list):
    _nDb = data_config["config_manager"]["database"]
    _nType = data_config["config_manager"]["dataType"]
    path = "%s%s-%s.%s" % (settings.DIR_FILE, _nDb, _nType, "zip")

    # caso não exista cria um novo diretório e se existir limpa o atual
    if not os.path.exists(settings.DIR_FILE):
        os.mkdir(settings.DIR_FILE)
    else:
        erase_path(settings.DIR_FILE)

    json_bytes = json.dumps(data_list).encode('utf-8')
    with gzip.GzipFile(path, 'w', compresslevel=4) as fout:
        fout.write(json_bytes)
    logging.info("* Saved as zip in "+path)


def read_unzip():
    file = file_name = glob.glob("%s*.zip" % (settings.DIR_FILE, ))
    logging.info(file)
    with gzip.open(file[0], 'r') as f:
        descompress_list = json.loads(f.read())
    return descompress_list


def erase_path(path):
    if settings.os.path.exists(path):
        file_name = glob.glob("%s*.*" % (settings.DIR_FILE, ))
        # logging.info(file_name)
        for fname in file_name:
            settings.os.remove(fname)
