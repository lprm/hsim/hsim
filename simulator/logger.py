import sentry_sdk
import settings


class SimulatorLogger():

    def __init__(self):
        if (settings.SENTRY_DSN):
            try:
                sentry_sdk.init(settings.SENTRY_DSN)
                self.enabled = True
            except:
                self.enabled = False
        else:
            self.enabled = False

    def capture_exception(self, exception):
        print(self.enabled)
        if self.enabled:
            sentry_sdk.capture_exception(exception)

logger = SimulatorLogger()
